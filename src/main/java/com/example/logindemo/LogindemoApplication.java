package com.example.logindemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogindemoApplication {
	// TODO Continue tutorial https://medium.com/@gustavo.ponce.ch/spring-boot-spring-mvc-spring-security-mysql-a5d8545d837d
	public static void main(String[] args) {
		SpringApplication.run(LogindemoApplication.class, args);
	}
}
